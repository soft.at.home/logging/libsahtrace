/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
   @file

   @brief
   SAH Trace interface implementation
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>
#include <sys/time.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>

#include <debug/sahtrace.h>
#include <debug/user_trace.h>

static char* categories[] = {"SYSTEM", "INTERNET", "TELEPHONY", "TV", "WIFI", "USB", "HOME_LAN"};

#if !defined(NO_DOXYGEN)
typedef struct _sah_trace_zone {
    char* name;
    sah_trace_level level;
    struct _sah_trace_zone* next;
} sah_trace_zone;

typedef struct {
    char format[32];
    bool local;
} sah_trace_timeformat_date_time_config_t;
typedef struct {
    sah_trace_timeformat type;
    sah_trace_timeformat_date_time_config_t dateTime;
} sah_trace_timeformat_config_t;
#endif

static int traceLevel = 0;
static sah_trace_type traceType = TRACE_TYPE_STDOUT;
static char* id = NULL;
static int opened = 0;
static sah_trace_timeformat_config_t timeFormat = {
    .type = TRACE_TIME_SYSTEM_SECONDS,
    .dateTime = {
        .format = "%Y-%m-%d %H:%M:%S", // e.g. 2024-02-26 11:45:23
        .local = false,
    }
};
static sah_trace_zone* zones = NULL;
struct timespec startTime = {0, 0};


/* function prototypes */
static void sahTraceOutput(int lvl, int traceLvl, const char* format, va_list vl);

static void sahTraceCOutput(int lvl, const char* cat, const char* format, va_list vl);

/**
   @brief
   Modify timespec a by subtracting timespec b
 */
static void timeSpecSubtract(struct timespec* a, struct timespec* b) {
    a->tv_nsec -= b->tv_nsec;
    if(a->tv_nsec < 0) {
        a->tv_nsec += 1000000000;
        a->tv_sec -= 1;
    }
    a->tv_sec -= b->tv_sec;
}

static void printTimeInSeconds(FILE* stream, struct timespec* ts) {
    fprintf(stream, "(%d.%3.3d)", (int) ts->tv_sec, (int) ts->tv_nsec / 1000000 /* ms */);
}

static void printTimeInFormat(FILE* stream, const char* format, struct timespec* now, bool local) {
    char str[64] = {0};
    struct tm* tm = local ? localtime(&now->tv_sec) : gmtime(&now->tv_sec);
    strftime(str, sizeof(str) - 1, format, tm);
    fprintf(stream, "%s ", str);
}

static void printTime(FILE* stream) {
    struct timespec now = {0, 0};
    clockid_t clock_id = CLOCK_REALTIME;
    if(timeFormat.type == TRACE_TIME_APP_SECONDS) {
        clock_id = CLOCK_MONOTONIC;
    }
    clock_gettime(clock_id, &now);

    switch(timeFormat.type) {
    case TRACE_TIME_SYSTEM_SECONDS:
        printTimeInSeconds(stream, &now);
        break;
    case TRACE_TIME_APP_SECONDS:
        timeSpecSubtract(&now, &startTime);
        printTimeInSeconds(stream, &now);
        break;
    case TRACE_TIME_SYSTEM_TIME:
        printTimeInFormat(stream, "(%H:%M:%S)", &now, true);
        break;
    case TRACE_TIME_SYSTEM_DATE_TIME:
        printTimeInFormat(stream, timeFormat.dateTime.format, &now, timeFormat.dateTime.local);
        break;
    default:
        break;
    }
}

static int sahTraceLvlToSyslogLvl(int lvl) {
    if(lvl == 0) {
        return LOG_CRIT;
    } else if(lvl <= 100) {
        return LOG_ERR;
    } else if(lvl <= 200) {
        return LOG_WARNING;
    } else if(lvl <= 300) {
        return LOG_NOTICE;
    } else if(lvl <= 400) {
        return LOG_INFO;
    } else {
        return LOG_DEBUG;
    }
}

static void sahTraceOutput(int lvl, int traceLvl, const char* format, va_list vl) {
    if((lvl <= traceLvl) && opened) {
        switch(traceType) {
        case TRACE_TYPE_SYSLOG:
        {
            int logLvl = sahTraceLvlToSyslogLvl(lvl);
            vsyslog(logLvl, format, vl);
        }
        break;
        case TRACE_TYPE_STDOUT:
            printTime(stdout);
            fprintf(stdout, "[%s] - ", id);
            vfprintf(stdout, format, vl);
            fprintf(stdout, "\n");
            break;
        case TRACE_TYPE_STDERR:
        case TRACE_TYPE_FILE:
            printTime(stderr);
            fprintf(stderr, "[%s] - ", id);
            vfprintf(stderr, format, vl);
            fprintf(stderr, "\n");
            break;
        default:
            break;
        }
    }
}

static void sahTraceCOutput(int lvl, const char* cat, const char* format, va_list vl) {
    if(opened) {
        switch(traceType) {
        case TRACE_TYPE_SYSLOG:
        {
            int logLvl = sahTraceLvlToSyslogLvl(lvl);
            char buf[256];
            sprintf(buf, "[USER][%s] - ", cat);
            strncat(buf, format, sizeof(buf) - strlen(buf) - 1);
            vsyslog(logLvl, &buf[0], vl);
        }
        break;
        case TRACE_TYPE_STDOUT:
            printTime(stdout);
            fprintf(stdout, "[USER][%s] - ", cat);
            vfprintf(stdout, format, vl);
            fprintf(stdout, "\n");
            break;
        case TRACE_TYPE_STDERR:
        case TRACE_TYPE_FILE:
            printTime(stderr);
            fprintf(stdout, "[USER][%s] - ", cat);
            vfprintf(stderr, format, vl);
            fprintf(stderr, "\n");
            break;
        default:
            break;
        }
    }
}

static int sahTraceGetZoneLevel(const char* name) {
    sah_trace_zone* zone = sahTraceGetZone(name);

    if(zone) {
        return zone->level;
    } else if(strcmp(name, "all")) {
        return sahTraceGetZoneLevel("all");
    } else {
        return -1;
    }
}

#ifdef CONFIG_SAH_USER_TRACES_ENABLED
static char* sahTraceGetCategory(sah_trace_cat cat) {
    return categories[cat];
}
#endif

static void sahTraceParseZoneLevel(char* zone, int* level) {
    char* env = zone;

    *level = 500;
    for(env = zone; *env != '\0'; env++) {
        if(*env == ':') {
            *level = atoi(env + 1);
            *env = '\0';
            break;
        }
    }

    /* Backward compatible, check if level was set in separate environment variable */
    char* lvl = getenv(zone);
    if(lvl) {
        *level = atoi(lvl);
    }
}

static void sahTraceToFile(const char* file) {
    int fd = open(file, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
    if(fd < 0) {
        fprintf(stderr, "unable to open trace file %s: %m, using stderr for tracing", file);
        fd = STDERR_FILENO;
    }
    if(dup2(fd, STDERR_FILENO) < 0) {
        fprintf(stderr, "unable to open trace file %s: %m, using stderr for tracing", file);
    }
    traceType = TRACE_TYPE_FILE;
}

/** @brief Searches in environment variable or file for the trace levels per trace zone
 *
 * example input file /etc/sah_trace_zones (note that "all" should come last)
 *                     pcb_plugin=all:400,pcb:400,DECTLAS:500
 *                     firewall_plugin=all:100,pcb>/root/fw.log
 *                     all=all:100
 * example environment variable (new format, single env-variable)
 *                     SAH_TRACE_ZONES=all:400,pcb:400,DECTLAS:500
 * example environment variable (old format, multiple env-variables)
 *                     SAH_TRACE_ZONES=all,pcb,DECTLAS  all=400  pcb=400  DECTLAS=500
 *
 * Note: to see which trace zones exist in an executable, run it first with "SAH_TRACE_ZONES=all500" and then only with the zones you want to log.
 * If not level is specified, it is set to 500.
 * The self zone is used to set the main level
 **/
static void sahTraceLoadZones(void) {
    char* env_zones = getenv("SAH_TRACE_ZONES");
    char buf[200];
    int level = 0;
    char* file = NULL;
    char* i;

    if((env_zones == NULL) || (*env_zones == '\0')) {
        FILE* fd = fopen("/etc/sah_trace_zones", "r");
        int idlen = strlen(id);
        if(fd) {
            /* Search for line that starts with the right id=... or "all=" */
            while(fgets(buf, sizeof(buf), fd) != NULL) {

                /* strip invalid chars from end of file */
                for(i = buf + strlen(buf) - 1; isspace(*i) && i >= buf; i--) {
                    *i = '\0';
                }

                /* look for file redirection */
                file = strchr(buf, '>');
                if(file != NULL) {
                    *(file++) = '\0';
                }

                if((buf[idlen] == '=') && !memcmp(id, buf, idlen)) { /* Found line matching our ID */
                    env_zones = &buf[idlen + 1];
                    break;
                } else if(!memcmp(buf, "all=", 4)) { /* Found a line that matches all ID's */
                    env_zones = &buf[4];
                    break;
                } else {
                    continue;
                }
            }
            fclose(fd);
        }
    }
    if(env_zones) {
        char* zone = strtok(env_zones, ",");
        while(zone) {
            sahTraceParseZoneLevel(zone, &level);
            if(strcmp("self", zone) == 0) {
                sahTraceSetLevel(level);
            } else {
                sahTraceAddZone(level, zone);
            }
            zone = strtok(NULL, ",");
        }
    }

    if(file) {
        sahTraceToFile(file);
    }
}

/**
   @ingroup sah_trace
   @{
 */

/**
   @deprecated
   Use @ref sahTraceOpen. This function will call sahTraceOpen with the output type set to TRACE_TYPE_STDOUT

   @brief
   Initialize the trace functionality.

   @details
   Initialize the trace functionality.

   @param ident The string  pointed to by ident is prepended to every message.
 */
/* deprecated function, use sahTraceOpen */
void sahTraceInitialize(const char* ident) {
    sahTraceOpen(ident, TRACE_TYPE_STDOUT);
}

/**
   @brief
   Opens and initializes the trace functionality

   @details
   Initializes and opens the trace functionality. Depending on the output "device" chosen
   initialization will be a bit different.
   When syslog is chosen, the syslog will be opened using the "openlog" system call
   ( openlog(ident,LOG_CONS,LOG_USER) ), and all messages/output will be send to the system log facility.
   On a windows system the @ref TRACE_TYPE_SYSLOG, will have no effect.
   An application can open the tracing only once. The trace options are set on a global application bases.
   Consecutive calls to this function will have no effect.

   @param ident The string  pointed to by ident is prepended to every message.
   @param type Specifies the output "device". This can be any of the @ref sah_trace_type values
 */
void sahTraceOpen(const char* ident, sah_trace_type type) {
    int flags = 0;

    if(opened == 0) {
        id = malloc(strlen(ident) + 1);
        clock_gettime(CLOCK_MONOTONIC, &startTime);
        switch(type) {
        case TRACE_TYPE_SYSLOG:
            openlog(ident, flags, LOG_USER);
            break;
        case TRACE_TYPE_STDOUT:
        case TRACE_TYPE_STDERR:
        case TRACE_TYPE_FILE:
            break;
        default:
            break;
        }
        strncpy(id, ident, strlen(ident) + 1);
        traceType = type;
        opened = 1;
        sahTraceLoadZones();
    }
}

/**
   @brief
   Opens and initializes the trace functionality

   @details
   Initializes and opens the trace functionality. The traces will be appended to the specified
   file. If the file can't be written to, the trace will be outputted to stderr.

   @param ident The string pointed to by ident is prepended to every message.
   @param file The file where to write the traces to.
 */
void sahTraceOpenFile(const char* ident, const char* file) {
    sahTraceToFile(file);
    sahTraceOpen(ident, TRACE_TYPE_FILE);
}

/**
   @brief
   Closes the tracing facility

   @details
   After calling this functions, no more traces will be visible.
   Tracing can be restarted by re-opening the tracing facility using @ref sahTraceOpen.
   If you only need to modify the current trace level, use @ref sahTraceSetLevel instead.
   When tracing was not opened before this function will have no effect.
 */
void sahTraceClose(void) {
    if(opened == 1) {
        closelog();
        opened = 0;
        free(id);
        sahTraceRemoveAllZones();
    }
}

/**
   @brief
   Indicates that the tracing facilities has been opened or not.

   @details
   Returns an indicator that indicates that tracing was opened or not.

   @return
   - 0 when tracing is closed (@ref sahTraceOpen was not called or @ref sahTraceClose was called)
   - 1 when tracing is opened (@ref sahTraceOpen was called)
 */
int sahTraceIsOpen(void) {
    return opened;
}

/**
   @brief
   Sets the timeformat for the messages.

   @details
   Each message will be prepended with a timestamp. This timestamp can be shown in different
   formats. See @ref sah_trace_timeformat for more information about the different formats.
   You do not need to close tracing before changing the timestamp format.

   @param f The timeformat.
 */
void sahTraceSetTimeFormat(sah_trace_timeformat f) {
    timeFormat.type = f;
}

/**
   @brief
   Sets the time format used by TRACE_TIME_SYSTEM_DATE_TIME

   @details
   The date time format follows the strftime format syntax

   @param format The date time format.
 */
void sahTraceSetDateTimeFormat(const char* format) {
    if(format == NULL) {
        return;
    }
    strncpy(timeFormat.dateTime.format, format, sizeof(timeFormat.dateTime.format) - 1);
}

/**
   @brief
   Set whether log date time timestamps should be in the local or UTC timezone

   @param local The local flag: true if local timezone
 */
void sahTraceSetDateTimeLocal(bool local) {
    timeFormat.dateTime.local = local;
}

/**
   @brief
   Sets the trace level

   @details
   When adding traces to your application or library, you have to specify the trace level for that message.
   See @ref sahTrace. The message will be printed to the output "device" only if the global trace level
   is higher then the level of the message. With this function you can set the global trace level.
   Predefined trace levels:
    - TRACE_LEVEL_FATAL_ERROR (0)
    - TRACE_LEVEL_ERROR (100)
    - TRACE_LEVEL_WARNING (200)
    - TRACE_LEVEL_NOTICE (300)
    - TRACE_LEVEL_APP_INFO (350)
    - TRACE_LEVEL_INFO (400)
    - TRACE_LEVEL_CALLSTACK (500)

   @param lvl Trace level
 */
void sahTraceSetLevel(int lvl) {
    traceLevel = lvl;
}

/**
   @brief
   Sets the trace level

   @details
   When adding traces to your application or library, you have to specify the trace level for that message.
   See @ref sahTrace. The message will be printed to the output "device" only if the global trace level
   is higher then the level of the message. With this function you can set the global trace level.
 */
int sahTraceLevel(void) {
    return traceLevel;
}

/**
   @brief
   Returns the identifier which was passed to @ref sahTraceOpen

   @details
   Returns the identifier which was passed to @ref sahTraceOpen

   @return
    - the identifier which was passed to @ref sahTraceOpen
 */
const char* sahTraceIdentifier(void) {
    return id;
}

/**
   @brief
   Adds a trace zone.

   @details
   Adding trace zones to the tracing facility, will help you in turning on or off certain
   messages. Typically, libraries will use the zone variants of the tracing macros or functions.
   Each library can define its own zone identifier(s). When these identifier(s) are known you can
   turn on/off the traces of this library.
   A zone can only be added once, if it was added before, the trace level of that zone will be changed.

   @param lvl The maximum output level for the zone that is added
   @param name The zone name for which messages has to be outputted
 */
/* This function is not thread safe */
/* only add zones when starting in a single threaded environment */
void sahTraceAddZone(int lvl, const char* name) {
    sah_trace_zone* newZone = sahTraceGetZone(name);
    if(!newZone) {
        /* create the new zone */
        newZone = malloc(sizeof(sah_trace_zone));
        if(!newZone) {
            return;
        }
        newZone->name = malloc(strlen(name) + 1);
        strncpy(newZone->name, name, strlen(name) + 1);
        newZone->next = zones;
        zones = newZone;
    }
    newZone->level = lvl;
}

/**
   @brief
   Removes a trace zone.

   @details
   After calling this function no more traces for the specified zone will be send to the output "device"

   @param name The zone name for which messages has to be outputted
 */
void sahTraceRemoveZone(const char* name) {
    sah_trace_zone* curZone = zones;
    sah_trace_zone* prevZone = NULL;

    /* go trough the zone list */
    while(curZone) {
        /* if we find the entry, remove it */
        if(strcmp(curZone->name, name) == 0) {
            if(prevZone) {
                prevZone->next = curZone->next;
            } else {
                zones = curZone->next;          /* first element */
            }
            free(curZone->name);
            free(curZone);
            return;
        }
        prevZone = curZone;
        curZone = curZone->next;
    }
}

/**
   @brief
   Removes all trace zones

   @details
   Removes all trace zones
 */
void sahTraceRemoveAllZones(void) {
    sah_trace_zone* curZone;

    while(zones) {
        /* find the last zone */
        curZone = zones->next;
        free(zones->name);
        free(zones);
        zones = curZone;
    }
}

/**
   @brief
   Get the first trace zone.

   @details
   Get the first trace zone.

   @return
    - An iterator to the first trace zone
    - NULL if no trace zones are available
 */
sah_trace_zone_it* sahTraceFirstZone(void) {
    return zones;
}

/**
   @brief
   Get the next trace zone.

   @details
   Get the next trace zone.

   @param ref A trace zone iterator as reference

   @return
    - An iterator to the next trace zone
    - NULL if no next trace zone is available
 */
sah_trace_zone_it* sahTraceNextZone(sah_trace_zone_it* ref) {
    if(!ref) {
        return NULL;
    }

    return ref->next;
}

/**
   @brief
   Get zone iterator by name.

   @details
   Get zone iterAtor by name.

   @param name name of the zone

   @return
    - An iterator to the zone
    - NULL if no zone is available with that name
 */
sah_trace_zone_it* sahTraceGetZone(const char* name) {
    sah_trace_zone* curZone = zones;

    /* go trough the zone list */
    while(curZone) {
        /* return the level if we find the entry */
        if(strcmp(curZone->name, name) == 0) {
            return curZone;
        }
        curZone = curZone->next;
    }
    return NULL;
}

/**
   @brief
   Get the name of a trace zone.

   @details
   Get the name of a trace zone.

   @param zone A trace zone iterator

   @return
    - Name of the trace zone referenced by the iterator
    - NULL if the reference is NULL
 */
const char* sahTraceZoneName(sah_trace_zone_it* zone) {
    if(!zone) {
        return NULL;
    }

    return zone->name;
}

/**
   @brief
   Get the level of a trace zone.

   @details
   Get the level of a trace zone.

   @param zone A trace zone iterator

   @return
    - Level of the trace zone referenced by the iterator
    - 0 if the reference is NULL
 */
sah_trace_level sahTraceZoneLevel(sah_trace_zone_it* zone) {
    if(!zone) {
        return 0;
    }

    return zone->level;
}

/**
   @brief
   Set the level of a trace zone.

   @details
   Set the level of a trace zone.

   @param zone A trace zone iterator
   @param lvl the new level of the trace zone
 */
void sahTraceZoneSetLevel(sah_trace_zone_it* zone, int lvl) {
    if(!zone) {
        return;
    }

    zone->level = lvl;
}

/**
   @brief
   Get the current trace type.

   @details
   Get the current trace type.

   @return
   The current trace type.
 */
sah_trace_type sahTraceType(void) {
    return traceType;
}


/**
   @brief
   Output a message to the opened "device"

   @details
   Like printf this function takes a format and any number of arguments.
   The level provided is matched against the global trace level. If the message level is higher then
   the set global level, the message will be ignored and not printed to the "device".
   It is better to use the SAH_TRACE_xxxx macros. These macros will add the function name, file name and line number to your message.

   @param lvl message trace level
   @param format message format string
 */
void sahTrace(int lvl, const char* format, ...) {
    va_list args;
    va_start(args, format);
    sahTracev(lvl, format, args);
    va_end(args);
}

/**
   @brief
   Output a message to the opened "device" with varargs

   @details
   Like sahTrace, but this function takes a format and a variable argument list.

   @param lvl message trace level
   @param format message format string
   @param ap variable argument list
 */
void sahTracev(int lvl, const char* format, va_list ap) {
    sahTraceOutput(lvl, traceLevel, format, ap);
}

/**
   @brief
   Output a message to the opened "device"

   @details
   Like printf this function takes a format and any number of arguments.
   When the zone was not added to the list of active zones, the message will not be printed.
   The level provided is matched against the level provided when the zone was added (see @ref sahTraceAddZone), when
   the set zone level is lower then the level provided, the message will be ignored and not printed to the "device".
   It is better to use the SAH_TRACEZ_xxxx macros. These macros will add the function name, file name and line number to your message.

   @param lvl message trace level
   @param zone message trace zone
   @param format message format string
 */
void sahTraceZ(int lvl, const char* zone, const char* format, ...) {
    va_list args;
    va_start(args, format);
    sahTracevZ(lvl, zone, format, args);
    va_end(args);
}

/**
   @brief
   Output a message to the opened "device" with varargs

   @details
   Like sahTraceZ, but this function takes a format and a variable argument list.

   @param lvl message trace level
   @param zone message trace zone
   @param format message format string
   @param ap variable argument list
 */
void sahTracevZ(int lvl, const char* zone, const char* format, va_list ap) {
    sahTraceOutput(lvl, sahTraceGetZoneLevel(zone), format, ap);
}

/**
   @brief
   Output a message to the opened "device"

   @details
   Like printf this function takes a format and any number of arguments.

   @param lvl message trace level
   @param format message format string
 */
void sahTraceC(int lvl, sah_trace_cat cat, const char* format, ...) {
    va_list args;
    va_start(args, format);
    sahTraceCv(lvl, cat, format, args);
    va_end(args);
}

/**
   @brief
   Output a message to the opened "device" with varargs

   @details
   Like sahTraceC, but this function takes a format and a variable argument list.

   @param lvl message trace level
   @param format message format string
   @param ap variable argument list
 */
void sahTraceCv(int lvl, sah_trace_cat cat, const char* format, va_list ap) {
#ifdef CONFIG_SAH_USER_TRACES_ENABLED
    sahTraceCOutput(lvl, sahTraceGetCategory(cat), format, ap);
#endif
}

/**
   @}
 */
