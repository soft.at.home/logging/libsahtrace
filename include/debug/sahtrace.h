/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__SAHTRACE_H__)
#define __SAHTRACE_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file

   @brief
   SAH Trace interface definition file
 */

/**
   @defgroup sah_trace SAH Tracing Facility
   @{

   @brief
   Soft At Home's Tracing Facility

   @details
   @section sah_trace_init Initializing the tracing facility
   Typically in each of your applications that uses SAH libraries you will initialize the tracing functionality.
   This is done by calling @ref sahTraceOpen, followed by @ref sahTraceSetLevel.\n
   Also trace zones can be specified using @ref sahTraceAddZone\n
   When @ref sahTraceClose is called, no more traces will be outputted. The global trace level can be changed
   at any given time using @ref sahTraceSetLevel. \n

   @section sah_trace_zones Trace Zones
   Zones are used as a filter. To see trace messages from a certain zone, the zone must be activated.
   This can be done by calling one or more times the @ref sahTraceAddZone function.\n

   @section sah_trace_messages Providing Trace Messages
   Use the SAH_TRACE_XXX macros to output a trace message to the output "device".\n
   If you want to output a message for a specific zone use the SAH_TRACEZ_XXX macros.

   @section sah_trace_threading SAH Trace and threads
   Non of the provided functions are thread safe.

   @section sah_trace_compiling SAH Trace at compile time
   If you want to have sah traces enabled in your application or library, compile your application
   or library with SAH_TRACES_ENABLED defined (-DSAHTRACES_ENABLED).\n
   Optionally you can specify at compile time the maximum trace level by defining the SAHTRACES_LEVEL
   (-DSAHTRACES_LEVEL=maxlevel).

 */

#include <stdarg.h>
#include <stdbool.h>

/**
   @brief
   Device that has to be opened

   @details
   Provide one of these values to the @ref sahTraceOpen function
 */
typedef enum
{
    TRACE_TYPE_SYSLOG, /**< Output to the system log*/
    TRACE_TYPE_STDOUT, /**< Output to the stdout */
    TRACE_TYPE_STDERR, /**< Output to the stderr */
    TRACE_TYPE_FILE,   /**< Output to a file */
} sah_trace_type;

/**
   @brief
   Predefined trace levels

   @details
   These can be used in all trace functions and macros
 */
typedef enum
{
    TRACE_LEVEL_FATAL_ERROR = 0, /**< Fatal error, it is not possible to turn these messages off */
    TRACE_LEVEL_ERROR = 100,     /**< Error message */
    TRACE_LEVEL_WARNING = 200,   /**< Warning message */
    TRACE_LEVEL_NOTICE = 300,    /**< Notice message */
    TRACE_LEVEL_APP_INFO = 350,  /**< Application info message */
    TRACE_LEVEL_INFO = 400,      /**< Info message (normally used by libraries) */
    TRACE_LEVEL_CALLSTACK = 500  /**< Callstack messages */
} sah_trace_level;

/**
   @brief
   Timestamp format

   @details
   Predefined timestamp formats
 */
typedef enum
{
    TRACE_TIME_SYSTEM_SECONDS,      /**< System time in seconds.milliseconds */
    TRACE_TIME_APP_SECONDS,         /**< Application time in seconds.milliseconds. The application time is relative to the time the application has been started */
    TRACE_TIME_SYSTEM_TIME,         /**< System time in human readable form (HH:MM:SS) */
    TRACE_TIME_SYSTEM_DATE_TIME,    /**< System date & time in human readable form (default: YYYY-MM-DD HH:MM:SS) */
} sah_trace_timeformat;

typedef struct _sah_trace_zone sah_trace_zone_it;

/**
   @}
 */

// deprecated functions
void sahTraceInitialize(const char* ident);

// tracing function
void sahTraceOpen(const char* ident, sah_trace_type type);
void sahTraceOpenFile(const char* ident, const char* file);
void sahTraceClose(void);
int sahTraceIsOpen(void);

void sahTraceSetTimeFormat(sah_trace_timeformat format);
void sahTraceSetDateTimeFormat(const char* format);
void sahTraceSetDateTimeLocal(bool local);

void sahTraceSetLevel(int tracelevel);
int sahTraceLevel(void);

const char* sahTraceIdentifier(void);

void sahTraceAddZone(int lvl, const char* name);
void sahTraceRemoveZone(const char* name);
void sahTraceRemoveAllZones(void);

sah_trace_zone_it* sahTraceFirstZone(void);
sah_trace_zone_it* sahTraceNextZone(sah_trace_zone_it* ref);
sah_trace_zone_it* sahTraceGetZone(const char* zone);
const char* sahTraceZoneName(sah_trace_zone_it* zone);
sah_trace_level sahTraceZoneLevel(sah_trace_zone_it* zone);
void sahTraceZoneSetLevel(sah_trace_zone_it* zone, int lvl);

sah_trace_type sahTraceType(void);

#if !defined(NO_DOXYGEN) // doxygen can not handle the attribute
void sahTrace(int tracelevel, const char* format, ...) __attribute__ ((format(printf, 2, 3)));
void sahTraceZ(int tracelevel, const char* zone, const char* format, ...) __attribute__ ((format(printf, 3, 4)));
#else
void sahTrace(int tracelevel, const char* format, ...);
void sahTraceZ(int tracelevel, const char* zone, const char* format, ...);
#endif
void sahTracev(int tracelevel, const char* format, va_list ap);
void sahTracevZ(int tracelevel, const char* zone, const char* format, va_list ap);

#define SAHTRACE_SOURCE_FORMAT       " - %s(%s@%s:%d)%s"
#define SAHTRACE_ZONE_FORMAT         "%s%-7.7s%s - "
#define SAHTRACE_MARK_FORMAT         "%s[%s]%s%s"

#define SAHTRACE_SOURCE              SAHTRACE_RESET, SAHTRACE_LBLUE, __func__, __FILE__, __LINE__, SAHTRACE_RESET
#define SAHTRACE_ZONE(zone)          SAHTRACE_BLUE, zone, SAHTRACE_RESET
#define SAHTRACE_NO_ZONE             SAHTRACE_BLUE, "", SAHTRACE_RESET

#define SAHTRACE_RESET   ""
#define SAHTRACE_RED     ""
#define SAHTRACE_LRED    ""
#define SAHTRACE_YELLOW  ""
#define SAHTRACE_GREEN   ""
#define SAHTRACE_LGREEN  ""
#define SAHTRACE_BLUE    ""
#define SAHTRACE_LBLUE   ""
#define SAHTRACE_BRIGHT  ""

#define SAHTRACE_NEUTRAL             "", "-", "", ""
#define SAHTRACE_FATAL_ERROR         "", "X", "", ""
#define SAHTRACE_ERROR               "", "x", "", ""
#define SAHTRACE_WARNING             "", "!", "", ""
#define SAHTRACE_NOTICE              "", "n", "", ""
#define SAHTRACE_APP_INFO            "", "I", "", ""
#define SAHTRACE_INFO                "", "i", "", ""

#define SAH_TRACE_DO_NOTHING

#define SAH_TRACE_FATAL(format, ...) sahTrace(TRACE_LEVEL_FATAL_ERROR, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_NO_ZONE, SAHTRACE_FATAL_ERROR, ## __VA_ARGS__, SAHTRACE_SOURCE)

#if defined(SAHTRACES_ENABLED)

#if !defined(SAHTRACES_LEVEL)
#define SAHTRACES_LEVEL SAHTRACES_LEVEL_DEFAULT
#endif

#define SAH_TRACE(lvl, format, ...) sahTrace(lvl, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_NEUTRAL, ## __VA_ARGS__, SAHTRACE_SOURCE)
#define SAH_TRACEZ(lvl, zone, format, ...) sahTraceZ(lvl, zone, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_NEUTRAL, ## __VA_ARGS__, SAHTRACE_SOURCE)

#if !defined(SAHTRACES_LEVEL) || (SAHTRACES_LEVEL >= 100)
#define SAH_TRACE_ERROR(format, ...) sahTrace(TRACE_LEVEL_ERROR, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_ERROR, ## __VA_ARGS__, SAHTRACE_SOURCE)
#define SAH_TRACEZ_ERROR(zone, format, ...) sahTrace(TRACE_LEVEL_ERROR, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_ERROR, ## __VA_ARGS__, SAHTRACE_SOURCE)
#else
#define SAH_TRACE_ERROR(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_ERROR(zone, format, ...) SAH_TRACE_DO_NOTHING
#endif

#if !defined(SAHTRACES_LEVEL) || (SAHTRACES_LEVEL >= 200)
#define SAH_TRACE_WARNING(format, ...) sahTrace(TRACE_LEVEL_WARNING, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_WARNING, ## __VA_ARGS__, SAHTRACE_SOURCE)
#define SAH_TRACEZ_WARNING(zone, format, ...) sahTrace(TRACE_LEVEL_WARNING, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_WARNING, ## __VA_ARGS__, SAHTRACE_SOURCE)
#else
#define SAH_TRACE_WARNING(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_WARNING(zone, format, ...) SAH_TRACE_DO_NOTHING
#endif

#if !defined(SAHTRACES_LEVEL) || (SAHTRACES_LEVEL >= 300)
#define SAH_TRACE_NOTICE(format, ...) sahTrace(TRACE_LEVEL_NOTICE, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_NOTICE, ## __VA_ARGS__, SAHTRACE_SOURCE)
#define SAH_TRACEZ_NOTICE(zone, format, ...) sahTraceZ(TRACE_LEVEL_NOTICE, zone, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_NOTICE, ## __VA_ARGS__, SAHTRACE_SOURCE)
#else
#define SAH_TRACE_NOTICE(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_NOTICE(zone, format, ...) SAH_TRACE_DO_NOTHING
#endif

#if !defined(SAHTRACES_LEVEL) || (SAHTRACES_LEVEL >= 350)
#define SAH_TRACE_APP_INFO(format, ...) sahTrace(TRACE_LEVEL_APP_INFO, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_APP_INFO, ## __VA_ARGS__, SAHTRACE_SOURCE)
#define SAH_TRACEZ_APP_INFO(zone, format, ...) sahTraceZ(TRACE_LEVEL_APP_INFO, zone, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_APP_INFO, ## __VA_ARGS__, SAHTRACE_SOURCE)
#else
#define SAH_TRACE_APP_INFO(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_APP_INFO(zone, format, ...) SAH_TRACE_DO_NOTHING
#endif

#if !defined(SAHTRACES_LEVEL) || (SAHTRACES_LEVEL >= 400)
#define SAH_TRACE_INFO(format, ...) sahTrace(TRACE_LEVEL_INFO, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_INFO, ## __VA_ARGS__, SAHTRACE_SOURCE)
#define SAH_TRACEZ_INFO(zone, format, ...) sahTraceZ(TRACE_LEVEL_INFO, zone, "%s%-7.7s%s - %s[%s]%s%s" format "%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_INFO, ## __VA_ARGS__, SAHTRACE_SOURCE)
#else
#define SAH_TRACE_INFO(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_INFO(zone, format, ...) SAH_TRACE_DO_NOTHING
#endif

#if !defined(SAHTRACES_LEVEL) || (SAHTRACES_LEVEL >= 500)
#define SAH_TRACE_IN() sahTrace(TRACE_LEVEL_CALLSTACK, "%s%-7.7s%s - >>>>>>>>>>%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_SOURCE)
#define SAH_TRACE_OUT() sahTrace(TRACE_LEVEL_CALLSTACK, "%s%-7.7s%s - <<<<<<<<<<%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(""), SAHTRACE_SOURCE)
#define SAH_TRACEZ_IN(zone) sahTraceZ(TRACE_LEVEL_CALLSTACK, zone, "%s%-7.7s%s - >>>>>>>>>>%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_SOURCE)
#define SAH_TRACEZ_OUT(zone) sahTraceZ(TRACE_LEVEL_CALLSTACK, zone, "%s%-7.7s%s - <<<<<<<<<<%s - %s(%s@%s:%d)%s", SAHTRACE_ZONE(zone), SAHTRACE_SOURCE)
#else
#define SAH_TRACE_IN() SAH_TRACE_DO_NOTHING
#define SAH_TRACE_OUT() SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_IN(zone) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_OUT(zone) SAH_TRACE_DO_NOTHING
#endif

#else

#if !defined(SAHTRACES_LEVEL)
#define SAHTRACES_LEVEL 0
#endif

#define SAH_TRACE(lvl, format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACE_ERROR(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACE_WARNING(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACE_NOTICE(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACE_APP_INFO(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACE_INFO(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACE_IN() SAH_TRACE_DO_NOTHING
#define SAH_TRACE_OUT() SAH_TRACE_DO_NOTHING

#define SAH_TRACEZ(lvl, zone, format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_ERROR(zone, format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_WARNING(zone, format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_NOTICE(format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_APP_INFO(zone, format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_INFO(zone, format, ...) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_IN(zone) SAH_TRACE_DO_NOTHING
#define SAH_TRACEZ_OUT(zone) SAH_TRACE_DO_NOTHING

#endif

#ifdef __cplusplus
}
#endif

#endif
